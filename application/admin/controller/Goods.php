<?php
namespace app\admin\controller;
use think\Db;

class Goods extends Base
{
	
	
	//商品列表//
	public function index(){
		$data=Db::name('goods')->select();
		return $this->fetch('index',['list'=>$data]);
	}
	
	
	
	//添加商品//
	public function add_goods(){
		if(request()->isAjax()){
            $param = input('post.');
            $param['add_time']=date('y-m-d h:i:s',time());
            if(Db::name('goods')->insert($param)){
            	return json(['code'=>1,'msg' =>'添加成功']);
            }else{
            	return json(['code'=>2,'msg' =>'添加失败']);
            }
            
        }
		
		$this->assign('position',Db::name('goodstype')->select());
		return $this->fetch();
	}
	
	
	//分类列表
	
	
	public function goodstype(){
		$data=Db::name('goodstype')->select();
		return $this->fetch('goodstype',['list'=>$data]);
	}
	
	
	//添加分类
	
	public function add_type(){
		if(request()->isAjax()){

            $param = input('post.');
            $param['add_time']=date('y-m-d h:i:s',time());
            $flag = Db::name('goodstype')->insert($param);
            return json(['code'=>1,'msg' =>'成功']);
        }

        return $this->fetch();
		
	}
	
	
	//删除分类
	
	public function delete($id){
		if(Db::name('goodstype')->where('id',$id)->delete()){
			return '删除成功';
		}else{
			return '删除失败';
		}
	}
	
	
	public function edit_type($id){
	$data=Db::name('goodstype')->find($id);
		if(request()->isAjax()){
            $param = input('post.');
            $flag = Db::name('goodstype')->where('id',$id)->update($param);
            return json(['code'=>1,'msg' =>'成功']);
        }
		return $this->fetch('edit_type',['data'=>$data]);
	}
}
?>
