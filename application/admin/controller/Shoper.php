<?php

namespace app\admin\controller;
use app\admin\model\ShoperModel;
use think\Db;

class Shoper extends Base
{ 
	public function index(){
		$list=Db::name('shop')->select();
		  return $this->fetch('index',['list'=>$list]);
	}

//编辑会员
  public function edit_member()
    {
        $member = new ShoperModel();
        if(request()->isAjax()){
            $param = input('post.');
            if(empty($param['password'])){
                unset($param['password']);
            }else{
                $param['password'] = md5(md5($param['password']) . config('auth_key'));
            }
            $flag = $member->editMember($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }

        $id = input('param.id');
		$me=$member->where('id',$id)->find();
        $this->assign('member',$me);
        return $this->fetch();
    }

}