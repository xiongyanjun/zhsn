<?php

return [
    //模板参数替换
    
    'view_replace_str' => array(
        '__CSS__' => '/public/static/admin/css',
        '__JS__'  => '/public/static/admin/js',
        '__IMG__' => '/public/static/admin/images',		
        '__LIB__' => '/public/static/admin',	
        '__PUB__' => '/public',	
    ),
];
