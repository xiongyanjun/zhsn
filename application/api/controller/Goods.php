<?php

namespace app\api\controller;
use think\Controller;
use think\Db;

/**
 * swagger: 广告
 */
class Goods
{
	/**
	 * get: 广告列表
	 * path: list
	 * method: list
	 * param: position - {int} 广告位
	 */
	public function get_goodslist()
    {     
    	$list=Db::name('goods')->where('is_sale',1)->select();
		return jsonp(['code'=>200,'list'=>$list]);
    }


}