<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:61:"D:\phpStudy\WWW\zhsn/application/admin\view\shoper\index.html";i:1497520822;s:62:"D:\phpStudy\WWW\zhsn/application/admin\view\public\header.html";i:1497247114;s:62:"D:\phpStudy\WWW\zhsn/application/admin\view\public\footer.html";i:1497245553;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/public/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/public/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/public/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/public/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/public/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/public/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/public/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/public/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- Panel Other -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>商家列表</h5>
        </div>
        <div class="ibox-content">
            <!--搜索框开始-->           
            <div class="row">
                <div class="col-sm-12">   
                                     
                    <form name="admin_list_sea" class="form-search" method="post" action="<?php echo url('index'); ?>">
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" id="key" class="form-control" name="key" value="" placeholder="输入需查询的商家账号/昵称/手机号" />   
                                <span class="input-group-btn"> 
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> 搜索</button> 
                                </span>
                            </div>
                        </div>
                    </form>                         
                </div>
            </div>
            <!--搜索框结束-->
            <div class="hr-line-dashed"></div>
            <div class="example-wrap">
                <div class="example">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="long-tr">
                                <th width="4%">ID</th>
                                <th width="9%">商家名称</th>
                                <th width="9%">商家类型</th>
                                <th width="5%">logo</th>
                                <th width="5%">联系人</th>
                                <th width="5%">联系电话</th>
                                <th width="5%">公司地址</th>
                                <th width="6%">主要经营</th>
                                <th width="5%">经营年限</th>
                                <th width="6%">状态</th>
                                <th width="10%">注册时间</th>
                                <th width="10%">操作</th>
                            </tr>
                        </thead>
                       
                            <tr class="long-td">
                            	 <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$d): $mod = ($i % 2 );++$i;?>
                                <td><?php echo $d['id']; ?></td>
                                <td><?php echo $d['company']; ?></td>
 
                                <td>
                                    <?php if($d['company_type']==1): ?>
                                        合作社
                                    <?php elseif($d['company_type']==2): ?>
                                        家庭企业
                                    <?php elseif($d['company_type']==3): ?>
                                        农庄
                                    <?php elseif($d['company_type']==4): ?>
            	  龙头企业	
                              <?php endif; ?>
                                </td>
                               
                                <td>
                                    <img src="/uploads/face/{d.head_img}" class="img-circle" style="width:35px;height:35px" onerror="this.src='/static/admin/images/head_default.gif'"/>                                  
                                </td>
                                <td><?php echo $d['contact']; ?></td>
                                <td><?php echo $d['phone']; ?></td>                                  
                                <td><?php echo $d['address']; ?></td>
                                <td><?php echo $d['main_sell']; ?></td>
                                <td><?php echo $d['year']; ?></td>                                                                
                                <td>                             
                                    <?php if($d['status']==1): ?>
                                        <a class="red" href="javascript:;" onclick="member_status({d.id});">
                                            <div id="zt{d.id}"><span class="label label-info">开业</span></div>
                                        </a>
                                     <?php elseif($d['status']==2): ?>
                                        <a class="red" href="javascript:;" onclick="member_status({d.id});">
                                            <div id="zt{d.id}"><span class="label label-info">停业</span></div>
                                        </a>
                                    <?php else: ?>
                                        <a class="red" href="javascript:;" onclick="member_status({d.id});">
                                            <div id="zt{d.id}"><span class="label label-danger">删除</span></div>
                                        </a>
                                    <?php endif; ?>                               
                                </td>
                                <td><?php echo $d['add_time']; ?></td> 
                                <td>                                  
                                    <a href="javascript:;" onclick="edit_member(<?php echo $d['id']; ?>)" class="btn btn-primary btn-outline btn-xs">
                                        <i class="fa fa-paste"></i> 编辑</a>&nbsp;&nbsp;
                                    <a href="javascript:;" onclick="del_member(<?php echo $d['id']; ?>)" class="btn btn-danger btn-outline btn-xs">
                                        <i class="fa fa-trash-o"></i> 删除</a>                                       
                                </td>
                            </tr>
                           
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                        <tbody id="list-content"></tbody>
                    </table>
                    <div id="AjaxPage" style=" text-align: right;"></div>
                    <div id="allpage" style=" text-align: right;"></div>
                </div>
            </div>
            <!-- End Example Pagination -->
        </div>
    </div>
</div>
<!-- End Panel Other -->
</div>

<!-- 加载动画 -->
<div class="spiner-example">
    <div class="sk-spinner sk-spinner-three-bounce">
        <div class="sk-bounce1"></div>
        <div class="sk-bounce2"></div>
        <div class="sk-bounce3"></div>
    </div>
</div>

<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>

<script type="text/javascript">
	//编辑会员
function edit_member(id){
	
    location.href = './edit_member/id/'+id+'.html';
}

//删除会员
function del_member(id){
    lunhui.confirm(id,'<?php echo url("del_member"); ?>');
}

//用户会员
function member_status(id){
    lunhui.status(id,'<?php echo url("member_status"); ?>');
}

</script>
</script>
</body>
</html>