<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:60:"D:\phpStudy\WWW\zhsn/application/admin\view\goods\index.html";i:1497535402;s:62:"D:\phpStudy\WWW\zhsn/application/admin\view\public\header.html";i:1497247114;s:62:"D:\phpStudy\WWW\zhsn/application/admin\view\public\footer.html";i:1497245553;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo config('WEB_SITE_TITLE'); ?></title>
    <link href="/public/static/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/public/static/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/public/static/admin/css/animate.min.css" rel="stylesheet">
    <link href="/public/static/admin/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/public/static/admin/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="/public/static/admin/css/plugins/switchery/switchery.css" rel="stylesheet">
    <link href="/public/static/admin/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/public/static/admin/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <style type="text/css">
    .long-tr th{
        text-align: center
    }
    .long-td td{
        text-align: center
    }
    </style>
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- Panel Other -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>产品列表</h5>
        </div>
        <div class="ibox-content">
            <!--搜索框开始-->           
           <div class="row">
                <div class="col-sm-12">   
                <div  class="col-sm-2" style="width: 100px">
                    <div class="input-group" >  
                        <a href="<?php echo url('add_goods'); ?>"><button class="btn btn-outline btn-primary" type="button">添加产品</button></a> 
                    </div>
                </div>                                            
                    <form name="admin_list_sea" class="form-search" method="post" action="<?php echo url('index'); ?>">
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" id="key" class="form-control" name="key" value="" placeholder="输入需查询的商品名称" />   
                                <span class="input-group-btn"> 
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> 搜索</button> 
                                </span>
                            </div>
                        </div>
                    </form>  
                     
                </div>
            </div>
            <!--搜索框结束-->
            <div class="hr-line-dashed"></div>
            <div class="example-wrap">
                <div class="example">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr class="long-tr">
                                <th width="4%">ID</th>
                                <th width="9%">商品名称</th>
                                <th width="9%">商品分类</th>
                                <th width="9%">商品价格</th>
                                <th width="5%">商品地址</th>
                                <th width="5%">商品图片</th>
                                <th width="5%">今日产量</th>
                                <th width="10%">操作</th>
                            </tr>
						</thead>
						<tbody>
                        	<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ad): $mod = ($i % 2 );++$i;?>
                        	<tr class="long-td">
                        		<td><?php echo $ad['id']; ?></td>
                        		<td><?php echo $ad['goodsname']; ?></td>
                        		<td><?php echo $ad['shopid']; ?></td>
                        		<td><?php echo $ad['price']; ?></td>
                        		<td><?php echo $ad['address']; ?></td>
                        		<td> 
                        			<img src="/public/uploads/images/<?php echo $ad['images']; ?>" style="height: 30px" onerror="this.src='/public/static/admin/images/no_img.jpg'"/>
                        		</td>
                        		<td><?php echo $ad['mamount']; ?></td>
                        		<td></td>
                        	</tr>
                        	<?php endforeach; endif; else: echo "" ;endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End Example Pagination -->
        </div>
    </div>
</div>
<!-- End Panel Other -->
</div>

<!-- 加载动画 -->


<script src="__JS__/jquery.min.js?v=2.1.4"></script>
<script src="__JS__/bootstrap.min.js?v=3.3.6"></script>
<script src="__JS__/content.min.js?v=1.0.0"></script>
<script src="__JS__/plugins/chosen/chosen.jquery.js"></script>
<script src="__JS__/plugins/iCheck/icheck.min.js"></script>
<script src="__JS__/plugins/layer/laydate/laydate.js"></script>
<script src="__JS__/plugins/switchery/switchery.js"></script><!--IOS开关样式-->
<script src="__JS__/jquery.form.js"></script>
<script src="__JS__/layer/layer.js"></script>
<script src="__JS__/laypage/laypage.js"></script>
<script src="__JS__/laytpl/laytpl.js"></script>
<script src="__JS__/lunhui.js"></script>
<script>
    $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
</script>
</body>
</html>