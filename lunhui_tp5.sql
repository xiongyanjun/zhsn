/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : lunhui_tp5

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-05-02 19:20:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for think_ad
-- ----------------------------
DROP TABLE IF EXISTS `think_ad`;
CREATE TABLE `think_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  `ad_position_id` varchar(10) DEFAULT NULL COMMENT '广告位',
  `link_url` varchar(128) DEFAULT NULL,
  `images` varchar(128) DEFAULT NULL,
  `start_date` date DEFAULT NULL COMMENT '开始时间',
  `end_date` date DEFAULT NULL COMMENT '结束时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `closed` tinyint(1) DEFAULT '0',
  `orderby` tinyint(3) DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_ad
-- ----------------------------
INSERT INTO `think_ad` VALUES ('24', '23', '1', '123', '20170416\\363c841674371a9e730e65a085fbdf18.jpg', '0000-00-00', '0000-00-00', '1', '0', '23');
INSERT INTO `think_ad` VALUES ('25', '123', '1', '213', '20170416\\d8f2098b4846f2e087cc2c5dd1575219.jpg', '2016-10-12', '2016-10-12', '1', '0', '100');
INSERT INTO `think_ad` VALUES ('26', '345', '1', '345', '20170416\\f59059c762d959f04f9226eb0c126987.jpg', '2016-10-25', '2016-10-20', '1', '0', '127');

-- ----------------------------
-- Table structure for think_admin
-- ----------------------------
DROP TABLE IF EXISTS `think_admin`;
CREATE TABLE `think_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_bin DEFAULT '' COMMENT '用户名',
  `password` varchar(32) COLLATE utf8_bin DEFAULT '' COMMENT '密码',
  `portrait` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '头像',
  `loginnum` int(11) DEFAULT '0' COMMENT '登陆次数',
  `last_login_ip` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '最后登录IP',
  `last_login_time` int(11) DEFAULT '0' COMMENT '最后登录时间',
  `real_name` varchar(20) COLLATE utf8_bin DEFAULT '' COMMENT '真实姓名',
  `status` int(1) DEFAULT '0' COMMENT '状态',
  `groupid` int(11) DEFAULT '1' COMMENT '用户角色id',
  `token` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of think_admin
-- ----------------------------
INSERT INTO `think_admin` VALUES ('1', 'admin', '218dbb225911693af03a713581a7227f', '20161122\\admin.jpg', '278', '0.0.0.0', '1493722370', 'admin', '1', '1', 'e96ed478dab8595a7dbda4cbcbee168f');
INSERT INTO `think_admin` VALUES ('9', 'tjl', 'ad2e48e7d1f92ba78d00cae476d8747e', '20161122\\ab9f9c492871857e1a6c5bc1c658ef7f.jpg', '18', '0.0.0.0', '1477140627', '田建龙', '1', '2', null);
INSERT INTO `think_admin` VALUES ('13', 'test', '218dbb225911693af03a713581a7227f', '20161122\\293c8cd05478b029a378ac4e5a880303.jpg', '1443', '0.0.0.0', '1487944038', 'test', '1', '4', null);
INSERT INTO `think_admin` VALUES ('14', 'shine', '5725323e51457c061867494f775d77dc', '20161122\\8a69f4c962e26265fd9f12efbff65013.jpg', '1', '0.0.0.0', '1479826965', '222222', '1', '2', null);

-- ----------------------------
-- Table structure for think_ad_position
-- ----------------------------
DROP TABLE IF EXISTS `think_ad_position`;
CREATE TABLE `think_ad_position` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '分类名称',
  `orderby` varchar(10) DEFAULT '100' COMMENT '排序',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_ad_position
-- ----------------------------
INSERT INTO `think_ad_position` VALUES ('1', '前台首页轮播', '1', '1477140627', '1487938246', '1');
INSERT INTO `think_ad_position` VALUES ('2', '前台首页右侧广告位', '2', '1477140627', '1477140627', '1');
INSERT INTO `think_ad_position` VALUES ('3', '前台首页底部广告位', '3', '1477140627', '1477140627', '1');

-- ----------------------------
-- Table structure for think_article
-- ----------------------------
DROP TABLE IF EXISTS `think_article`;
CREATE TABLE `think_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '文章逻辑ID',
  `title` varchar(128) NOT NULL COMMENT '文章标题',
  `cate_id` int(11) NOT NULL DEFAULT '1' COMMENT '文章类别',
  `photo` varchar(64) DEFAULT '' COMMENT '文章图片',
  `remark` varchar(256) DEFAULT '' COMMENT '文章描述',
  `keyword` varchar(32) DEFAULT '' COMMENT '文章关键字',
  `content` text NOT NULL COMMENT '文章内容',
  `views` int(11) NOT NULL DEFAULT '1' COMMENT '浏览量',
  `status` tinyint(1) DEFAULT NULL,
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '文章类型',
  `is_tui` int(1) DEFAULT '0' COMMENT '是否推荐',
  `from` varchar(16) NOT NULL DEFAULT '' COMMENT '来源',
  `writer` varchar(64) NOT NULL COMMENT '作者',
  `ip` varchar(16) NOT NULL,
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `a_title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of think_article
-- ----------------------------
INSERT INTO `think_article` VALUES ('5', 'PHP初学者必须掌握的10个知识点', '5', './images/2016-01-04/5689cee8cf466.jpg', '这里总结了PHP初学者容易感到困惑的10个问题，供大家参考。', 'PHP', '<p style=\"text-align:center\"><img alt=\"a8d43f7egw1evreghrn9sj20rs0eon1u.jpg\" src=\"/Upload/20150905/1441435048306230.jpg\" title=\"1441435048306230.jpg\"/></p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">这里总结了PHP初学者容易感到困惑的10个问题，供大家参考。</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">1、页面之间无法传递变量</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">get,post,session在最新的php版本中自动全局变量是关闭的，所以要从上一页面取得提交过来得变量要使用$_GET[&#39;foo&#39;],$_POST[&#39;foo&#39;],$_SESSION[&#39;foo&#39;]来得到。当然也可以修改自动全局变量为开(php.ini改为register_globals = On);考虑到兼容性，还是强迫自己熟悉新的写法比较好。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">注：PHP中的超全局变量</strong></p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">从PHP 4.2.0 开始，register_globals 的默认值为 off，这样一来，以前的很多可以直接使用的变量，如 $PHP_SELF 或者你设定的SESSION变量都不能用 “$变量名”的形式访问了，这可能会给你带来很多不变，但却有助于安全性的提高。访问这些变量，你需要使用PHP 超全局变量，如下：</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_SERVER</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">变量由 Web 服务器设定或者直接与当前脚本的执行环境相关联。类似于旧数组 $HTTP_SERVER_VARS 数组。以前的$PHP_SELF对应$_SERVER[&#39;PHP_SELF&#39;]，你可以使用phpinfo来查看你的$_SERVER变量。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_GET</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">经由 HTTP GET 方法提交至脚本的变量。类似于旧数组 $HTTP_GET_VARS 数组。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_POST</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">经由 HTTP POST 方法提交至脚本的变量。类似于旧数组 $HTTP_POST_VARS 数组。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_COOKIE</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">经由 HTTP Cookies 方法提交至脚本的变量。类似于旧数组 $HTTP_COOKIE_VARS 数组。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_SESSION</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">当前注册给脚本会话的变量。类似于旧数组 $HTTP_SESSION_VARS 数组。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_FILES</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">经由 HTTP POST 文件上传而提交至脚本的变量。类似于旧数组 $HTTP_POST_FILES 数组。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_ENV</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">执行环境提交至脚本的变量。类似于旧数组 $HTTP_ENV_VARS 数组。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">对于$_FILES变量：(文件域字段为“myfile”)</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_FILES[&#39;myfile&#39;][&#39;name&#39;]</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">客户端机器文件的原名称(包括路径)。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_FILES[&#39;myfile&#39;][&#39;type&#39;]</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">文件的 MIME 类型，需要浏览器提供该信息的支持，例如“image/gif”。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_FILES[&#39;myfile&#39;][&#39;size&#39;]</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">已上传文件的大小，单位为字节。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_FILES[&#39;myfile&#39;][&#39;tmp_name&#39;]</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">文件被上传后在服务端储存的临时文件名(包括路径)。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">$_FILES[&#39;myfile&#39;][&#39;error&#39;]</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">和该文件上传相关的错误代码。[&#39;error&#39;] 是在 PHP 4.2.0 版本中增加的。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">当 php.ini 中的 register_globals 被设置为 on 时，$myfile_name 等价于 $_FILES[&#39;myfile&#39;][&#39;name&#39;]，$myfile_type 等价于 $_FILES[&#39;myfile&#39;][&#39;type&#39;]等。</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">2、win32下的session不能正常工作</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">php.ini默认的session.save_path = /tmp</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">这显然是linux下的配置，win32下php无法读写session文件导致session无法使用，把它改成一个绝对路径就可以了，例如session.save_path = c:windowstemp。</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">3、显示错误信息</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">当php.ini的display_errors = On并且error_reporting = E_ALL时，将显示所有的错误和提示，调试的时候最好打开以便纠错，如果你用以前php写法错误信息多半是关于未定义变量的。变量在赋值以前调用会有提示，解决办法是探测或者屏蔽，例如显示$foo，可以if(isset($foo)) echo$foo 或者echo @$foo</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">4、header already sent</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">这个错误通常会在你使用HEADER的时候出现，他可能是几种原因：1，你在使用HEADER前PRING或者ECHO了2.你当前文件前面有空行3.你可能INCLUDE了一个文件,该文件尾部有空行或者输出也会出现这种错误。</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">5、更改php.ini后没有变化</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">重新启动web server，比如IIS，Apache等等，然后才会应用最新的设置。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">6、有时候sql语句不起作用，对数据库操作失败。最简便的调试方法，echo那句sql，看看变量的值是否能得到。</strong></p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">7、include和require的区别</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">两者没有太大的区别，如果要包含的文件不存在，include提示notice，然后继续执行下面的语句，require提示致命错误并且退出。根据测试，win32平台下它们都是先包含后执行，所以被包含文件里最好不要再有include或require语句，这样会造成目录混乱。或许*nux下情况不同，暂时还没测试。如果一个文件不想被包含多次可以使用include_once或require_once## 读取，写入文档数据：</p><pre class=\"brush: php; gutter: false; first-line: 1\" style=\"margin: 15px auto; padding: 10px 15px; word-break: break-all; word-wrap: break-word; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: 20px; font-family: &#39;courier new&#39;; border-width: 1px 1px 1px 4px; border-style: solid; border-color: rgb(221, 221, 221); background-color: rgb(251, 251, 251); color: rgb(68, 68, 68); letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-position: initial initial; background-repeat: initial initial;\">function&nbsp;r($file_name)&nbsp;{\r\n$filenum=@fopen($file_name,&quot;r&quot;);\r\n@flock($filenum,LOCK_SH);\r\n$file_data=@fread($filenum,filesize($file_name));\r\n@fclose($filenum);\r\nreturn&nbsp;$file_data;\r\n}\r\nfunction&nbsp;w($file_name,$data,$method=&quot;w&quot;){\r\n$filenum=@fopen($file_name,$method);\r\nflock($filenum,LOCK_EX);\r\n$file_data=fwrite($filenum,$data);\r\nfclose($filenum);\r\nreturn&nbsp;$file_data;\r\n}</pre><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">8、isset和empty的区别</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">两者都是测试变量用的，但是isset是测试变量是否被赋值，而empty是测试一个已经被赋值的变量是否为空。如果一个变量没被赋值就引用在php里是被允许的,但会有notice提示。如果一个变量被赋空值，$foo=”&quot;或者$foo=0或者 $foo=false,那么empty($foo)返回真，isset($foo)也返回真，就是说赋空值不会注销一个变量。要注销一个变量，可以用 unset($foo)或者$foo=NULL。</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">9、mysql查询语句包含有关键字</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">php查询mysql的时候，有时候mysql表名或者列名会有关键字，这时候查询会有错误。例如表名是order,查询时候会出错，简单的办法是sql语句里表名或者列名加上`[tab键上面]来加以区别，例如select * from `order`。</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">10、通过HTTP协议一次上传多个文件的方法</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">有两个思路，是同一个方法的两种实现。具体程序还需自己去设计</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">1. 在form中设置多个文件输入框，用数组命名他们的名字，如下：</strong></p><pre class=\"brush: html; gutter: false; first-line: 1\" style=\"margin: 15px auto; padding: 10px 15px; word-break: break-all; word-wrap: break-word; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: 20px; font-family: &#39;courier new&#39;; border-width: 1px 1px 1px 4px; border-style: solid; border-color: rgb(221, 221, 221); background-color: rgb(251, 251, 251); color: rgb(68, 68, 68); letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-position: initial initial; background-repeat: initial initial;\"><br/></pre><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">这样，在服务器端做以下测试</strong></p><pre class=\"brush: php; gutter: false; first-line: 1\" style=\"margin: 15px auto; padding: 10px 15px; word-break: break-all; word-wrap: break-word; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: 20px; font-family: &#39;courier new&#39;; border-width: 1px 1px 1px 4px; border-style: solid; border-color: rgb(221, 221, 221); background-color: rgb(251, 251, 251); color: rgb(68, 68, 68); letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-position: initial initial; background-repeat: initial initial;\">echo&nbsp;&quot;&quot;;\r\nprint_r($_FILES);\r\necho&nbsp;&quot;&quot;;</pre><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">2. 在form中设置多个文件输入框，但名字不同，如下：</strong></p><pre class=\"brush: php; gutter: false; first-line: 1\" style=\"margin: 15px auto; padding: 10px 15px; word-break: break-all; word-wrap: break-word; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: 20px; font-family: &#39;courier new&#39;; border-width: 1px 1px 1px 4px; border-style: solid; border-color: rgb(221, 221, 221); background-color: rgb(251, 251, 251); color: rgb(68, 68, 68); letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-position: initial initial; background-repeat: initial initial;\"><br/></pre><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\"><strong style=\"margin: 0px; padding: 0px;\">在服务器端做同样测试：</strong></p><pre class=\"brush: php; gutter: false; first-line: 1\" style=\"margin: 15px auto; padding: 10px 15px; word-break: break-all; word-wrap: break-word; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: 20px; font-family: \" courier=\"\" border-width:=\"\" 1px=\"\" border-style:=\"\" border-color:=\"\" background-color:=\"\" color:=\"\" letter-spacing:=\"\" text-align:=\"\" text-indent:=\"\" text-transform:=\"\" word-spacing:=\"\">echo&nbsp;&quot;&quot;;\r\nprint_r($_FILES);\r\necho&nbsp;&quot;&quot;;</pre>', '441', null, '1', '1', 'Win 8.1', '轮回', '124.152.7.106', '1441435103', '1452229136');
INSERT INTO `think_article` VALUES ('11', '编写高质量的代码，从命名入手', '1', './images/2016-01-04/5689ce8fcd0e2.jpg', '笔者从事开发多年，有这样一种感觉，查看一些开源项目，如Spring、Apache Common等源码是一件赏心悦目的事情，究其原因，无外两点：1）代码质量非常高；2）命名特别规范（这可能跟老外的英语水平有关）。', '代码', '<p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">笔者从事开发多年，有这样一种感觉，查看一些开源项目，如Spring、Apache Common等源码是一件赏心悦目的事情，究其原因，无外两点：1）代码质量非常高；2）命名特别规范（这可能跟老外的英语水平有关）。</p><p style=\"text-align:center\"><br/></p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">要写高质量的代码，不是一件容易的事，需要长年累月的锻炼，是一个量变到质变的过程，但要写好命名，只需要有比较好的英语语法基础和一种自我意识即可轻松达到。<strong style=\"margin: 0px; padding: 0px;\">本博文将会结合本人的开发经验，总结出若干命名规则，这些命名规则纯属个人的使用习惯，不代表是一种理想的规则，在这里列举出来，供大家交流讨论。</strong></p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">1.切忌使用没有任何意义的英语字母进行命名</h2><pre class=\"brush: java; gutter: true\" style=\"margin: 15px auto; padding: 10px 15px; word-break: break-all; word-wrap: break-word; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: 20px; font-family: &#39;courier new&#39;; border-width: 1px 1px 1px 4px; border-style: solid; border-color: rgb(221, 221, 221); background-color: rgb(251, 251, 251); color: rgb(68, 68, 68); letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-position: initial initial; background-repeat: initial initial;\">&nbsp;for(int&nbsp;i=0;&nbsp;i&lt;10;&nbsp;i++)&nbsp;{\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...\r\n&nbsp;&nbsp;&nbsp;&nbsp;}</pre><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">这是在很多教Java基本语法的书上常见的代码片断，作为教学材料，这样写无可厚非，但作为真正的代码编写，<span class=\"wp_keywordlink\"><a href=\"http://www.codeceo.com/\" title=\"程序员\" target=\"_blank\" style=\"color: rgb(0, 136, 219); text-decoration: none; cursor: pointer;\">程序员</a></span>必须要养成良好的习惯，不要使用这种没有任何含义的命名方式，这里可以使用“index”。</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">2.切忌使用拼音，甚至是拼音首字母组合</h2><pre class=\"brush: c; gutter: true\" style=\"margin: 15px auto; padding: 10px 15px; word-break: break-all; word-wrap: break-word; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: 20px; font-family: &#39;courier new&#39;; border-width: 1px 1px 1px 4px; border-style: solid; border-color: rgb(221, 221, 221); background-color: rgb(251, 251, 251); color: rgb(68, 68, 68); letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-position: initial initial; background-repeat: initial initial;\">cishu&nbsp;=5;&nbsp;//&nbsp;循环的次数\r\nzzje&nbsp;=&nbsp;1000.00&nbsp;//&nbsp;转账金额</pre><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">笔者在做代码检查的时候，无数次遇到过这样的命名，使人哭笑不得</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">3.要使用英文，而且要使用准确的英语，无论是拼写还是语法</h2><ul class=\" list-paddingleft-2\" style=\"list-style-type: none;\"><li><p>名词单数，必须使用单数英文，如Account、Customer。</p></li><li><p>对于数组，列表等对象集合的命名，必须使用复数，而且最好按照英文的语法基础知识使用准确的复数形式，如 List<account>accounts、Set<strategy>strategies。</strategy></account></p></li><li><p>对于boolean值的属性，很多开发人员习惯使用isXXX，如isClose（是否关闭），但这里有两点建议：1）最好不要带“is”，因为JavaBean的规范，为属性生成get/set方法的时候，会用“get/set/is”，上面的例子，生成get/set方法就会变成“getIsClose/isIsClose/getIsClose”，非常别扭；2）由于boolean值通常反映“是否”，所以准确的用法，应该是是用“形容词”，上面的例子，最终应该被改为 closed，那么get/set方法就是“getClosed/isColsed/setClosed”，非常符合英语阅读习惯。</p></li></ul><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">4.方法名的命名，需要使用“动宾结构短语”或“是动词+表语结构短语”</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">笔者曾看到过千奇百怪的方法命名，有些使用名词，有些甚至是“名词+动词”，而且，如果宾语是一个对象集合，还是最好使用复数：</p><pre class=\"brush: java; gutter: true\" style=\"margin: 15px auto; padding: 10px 15px; word-break: break-all; word-wrap: break-word; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: 20px; font-family: &#39;courier new&#39;; border-width: 1px 1px 1px 4px; border-style: solid; border-color: rgb(221, 221, 221); background-color: rgb(251, 251, 251); color: rgb(68, 68, 68); letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-position: initial initial; background-repeat: initial initial;\">createOrder(Order&nbsp;order)&nbsp;//good\r\norderCreate(Order&nbsp;order)&nbsp;//bad\r\nremoveOrders(List&nbsp;orders)&nbsp;//good\r\nremoveOrder(List&nbsp;order)&nbsp;//bad</pre><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">5.对于常见的“增删改查”方法，命名最好要谨慎：</h2><ul class=\" list-paddingleft-2\" style=\"list-style-type: none;\"><li><p>增加：最常见使用create和add，但最好根据英语的语义进行区分，这有助于理解，create代表创建，add代表增加。比如，要创建一个Student，用createStudent要比用addStudent好，为什么？想想如果有个类叫Clazz(班级，避开Java关键字），现在要把一个Student加入到一个Clazz，Clazz很容易就定义了一个 addStudent（Student student)的方法，那么就比较容易混淆。</p></li><li><p>修改：常见的有alter、update、modify，个人觉得modify最准确。</p></li><li><p>查询：对于获取单个对象，可以用get或load，但个人建议用get，解释请见第7点的说明，对于不分条件列举，用list，对于有条件查询，用search（最好不要用find，find在英文了强调结果，是“找到”的意思，你提供一个“查询”方法，不保证输入的条件总能“找到”结果）。</p></li><li><p>删除：常见的有delete和remove，但删除建议用delete，因为remove有“移除”的意思，参考Clazz的例子就可以理解，从班级移除一个学生，会用removeStudent。</p></li></ul><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">6.宁愿方法名冗长，也不要使用让人费解的简写</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">笔者曾经遇到一个方法，判断“支付账户是否与收款账户相同”，结果我看到一个这样的命名：</p><pre class=\"brush: java; gutter: true\" style=\"margin: 15px auto; padding: 10px 15px; word-break: break-all; word-wrap: break-word; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: 20px; font-family: &#39;courier new&#39;; border-width: 1px 1px 1px 4px; border-style: solid; border-color: rgb(221, 221, 221); background-color: rgb(251, 251, 251); color: rgb(68, 68, 68); letter-spacing: normal; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-position: initial initial; background-repeat: initial initial;\">checkIsOrderingAccCollAccSame(...)&nbsp;//&nbsp;很难理解，我马上把它改为：\r\nisOrderingAccountSameAsCollectionAccount(...)&nbsp;//&nbsp;虽然有点长，但非常容易阅读，而且这种情况总是出现得比较少。</pre><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">7.如果你在设计业务系统，最好不要使用技术化的术语去命名</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">笔者曾经工作的公司曾经制订这样的命名规则，接口必须要以“I”开头，数据传输对象必须以“DTO”作为后缀，数据访问对象必须以“DAO”作为后缀，领域对象必须以“DO”作为后缀，我之所以不建议这种做法，是希望设计人员从一开始就引导开发人员，要从“业务”出发考虑问题，而不要从“技术”出发。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">所以，接口不需要非得以“I”开头，只要其实现类以“Impl”结尾即可（注：笔者认为接口是与细节无关的，与技术无关，但实现类是实现相关的，用技术化术语无可口非），而数据传输对象，其实无非就是保存一个对象的信息，因此可以用“**Info”，如CustomerInfo，领域对象本身就是业务的核心，所以还是以其真实名称出现，比如Account、Customer，至于“DAO”，这一个词来源于J2ee的<span class=\"wp_keywordlink\"><a href=\"http://www.codeceo.com/article/category/develop/design-patterns\" title=\"设计模式\" target=\"_blank\" style=\"color: rgb(0, 136, 219); text-decoration: none; cursor: pointer;\">设计模式</a></span>，笔者在之前的项目使用“***Repository”命名，意味“***的仓库”，如AccountRepository.</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">关于“Repository”这个词的命名，是来源于Eric Evans的《Domain-Driven Design》一书的仓库概念，Eric Evans对Repository的概念定义是：领域对象的概念性集合，个人认为这个命名非常的贴切，它让程序员完全从技术的思维中摆脱出来，站在业务的角度思考问题。说到这里，可能有人会反驳：像Spring、Hibernate这些优秀的框架，不是都在用“I”作为接口开头，用“DAO”来命名数据访问对象吗？没错！但千万别忽略了语义的上下文，Spring、Hibernate框架都是纯技术框架，我这里所说的场景是设计业务系统。</p><h2 style=\"margin: 30px 0px 14px; padding: 0px 0px 5px; color: rgb(34, 34, 34); border-bottom-color: rgb(238, 238, 238); border-bottom-width: 1px; border-bottom-style: solid; font-size: 18px; font-family: &#39;microsoft yahei&#39;; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">8.成员变量不要重复类的名称</h2><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">例如，很多人喜欢在Account对象的成员变量中使用accountId，accountNumber等命名，其实没有必要，想想成员变量不会鼓孤立的存在，你引用accountId，必须是account.accountId，用account.id已经足够清晰了。</p><p style=\"margin: 0px 0px 15px; padding: 0px; color: rgb(68, 68, 68); font-family: &#39;microsoft yahei&#39;; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 25px; orphans: auto; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;\">“勿以善小而不为，勿以恶小而为之”、“细节决定成败”，有太多的名言告诉我们，要注重细节。一个优秀的程序员，必须要有坚实的基础，而对于命名规则这样容易掌握的基础，我们何不现行？</p><p><br/></p>', '377', null, '1', '1', 'Win 8.1', '轮回', '124.152.7.106', '1441598331', '1451871892');
INSERT INTO `think_article` VALUES ('52', 'PHP实现时间轴函数', '5', '20170416\\20bda129efdb56d4bee0629d616a2d99.jpg', '本文介绍如何用PHP将时间显示为“刚刚”、“5分钟前”、“昨天10:23”等时间轴形式，而不是直接显示具体日期和时间。', '时间轴', '<p>本文将介绍如何实现基于时间轴的时间的转换。</p><p>首先我们要明白时间的几个函数：</p><p>time():返回当前的 Unix 时间戳</p><p>date():格式化一个本地时间／日期。</p><p>应用举例：</p><pre style=\"margin: 6px auto; padding: 4px; border-width: 1px 1px 1px 5px; border-style: solid; border-color: rgb(215, 215, 215); font-size: 12px; white-space: pre-wrap; word-wrap: break-word; color: rgb(66, 66, 66); line-height: 21px; background-color: rgb(250, 250, 250);\"><code class=\"php\" style=\"margin: 0px; padding: 0px;\">date(<span class=\"php__string2\" style=\"margin: 0px; padding: 0px; color: fuchsia;\">&quot;Y-m-d&nbsp;H:i:s&quot;</span>,time());&nbsp;<br style=\"margin: 0px; padding: 0px;\"/></code></pre><p>格式化当前时间，输出：2010-10-11 05:27:35</p><p>strtotime():将任何英文文本的日期时间描述解析为 Unix 时间戳。</p><p>应用举例：</p><pre style=\"margin: 6px auto; padding: 4px; border-width: 1px 1px 1px 5px; border-style: solid; border-color: rgb(215, 215, 215); font-size: 12px; white-space: pre-wrap; word-wrap: break-word; color: rgb(66, 66, 66); line-height: 21px; background-color: rgb(250, 250, 250);\"><code class=\"php\" style=\"margin: 0px; padding: 0px;\"><span class=\"php__keyword\" style=\"margin: 0px; padding: 0px; color: navy; font-weight: bold;\">echo</span>&nbsp;strtotime(<span class=\"php__string2\" style=\"margin: 0px; padding: 0px; color: fuchsia;\">&quot;+1&nbsp;day&quot;</span>),&nbsp;<span class=\"php__string2\" style=\"margin: 0px; padding: 0px; color: fuchsia;\">&quot;n&quot;</span>;&nbsp;<br style=\"margin: 0px; padding: 0px;\"/></code></pre><p>输出1天前的时间戳：1286861475</p><p>date_default_timezone_set():设定要用的默认时区。</p><p>一般我们设置北京时间：date_default_timezone_set(&quot;PRC&quot;);</p><p>理解上面几个函数后我们来写时间轴函数：</p><p>该函数的原理就是将系统当前时间与目标时间比较，得到一个差值，再将差值与时间范围（转换成秒）比较，根据其处在时间轴的范围输出不同的结果（如：5分钟前）。为了便于计算，我们将时间都转换成Unix时间戳。</p><pre class=\"brush:php;toolbar:false\">function&nbsp;tranTime($time)&nbsp;{&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;$rtime&nbsp;=&nbsp;date(&quot;Y-m-d&nbsp;H:i&quot;,&nbsp;$time);&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;$htime&nbsp;=&nbsp;date(&quot;H:i&quot;,&nbsp;$time);&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;$time&nbsp;=&nbsp;time()&nbsp;-&nbsp;$time;&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;($time&nbsp;&lt;&nbsp;60)&nbsp;{&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$str&nbsp;=&nbsp;&#39;刚刚&#39;;&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;elseif&nbsp;($time&nbsp;&lt;&nbsp;60&nbsp;*&nbsp;60)&nbsp;{&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$min&nbsp;=&nbsp;floor($time&nbsp;/&nbsp;60);&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$str&nbsp;=&nbsp;$min&nbsp;.&nbsp;&#39;分钟前&#39;;&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;elseif&nbsp;($time&nbsp;&lt;&nbsp;60&nbsp;*&nbsp;60&nbsp;*&nbsp;24)&nbsp;{&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$h&nbsp;=&nbsp;floor($time&nbsp;/&nbsp;(60&nbsp;*&nbsp;60));&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$str&nbsp;=&nbsp;$h&nbsp;.&nbsp;&#39;小时前&nbsp;&#39;&nbsp;.&nbsp;$htime;&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;elseif&nbsp;($time&nbsp;&lt;&nbsp;60&nbsp;*&nbsp;60&nbsp;*&nbsp;24&nbsp;*&nbsp;3)&nbsp;{&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$d&nbsp;=&nbsp;floor($time&nbsp;/&nbsp;(60&nbsp;*&nbsp;60&nbsp;*&nbsp;24));&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if&nbsp;($d&nbsp;==&nbsp;1)&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$str&nbsp;=&nbsp;&#39;昨天&nbsp;&#39;&nbsp;.&nbsp;$rtime;&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;else&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$str&nbsp;=&nbsp;&#39;前天&nbsp;&#39;&nbsp;.&nbsp;$rtime;&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;else&nbsp;{&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$str&nbsp;=&nbsp;$rtime;&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;\n&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;$str;&nbsp;\n}</pre><p>&nbsp; &nbsp; 函数tranTime()中的参数$time必须为Unix时间戳，如果不是请先用strtotime()将其转换成Unix时间戳。上面的代码一看就明白了，不用再多述。</p><p>调用函数，直接输出：</p><pre style=\"margin: 6px auto; padding: 4px; border-width: 1px 1px 1px 5px; border-style: solid; border-color: rgb(215, 215, 215); font-size: 12px; white-space: pre-wrap; word-wrap: break-word; color: rgb(66, 66, 66); line-height: 21px; background-color: rgb(250, 250, 250);\"><code class=\"php\" style=\"margin: 0px; padding: 0px;\"><span class=\"php__keyword\" style=\"margin: 0px; padding: 0px; color: navy; font-weight: bold;\">$</span><span class=\"php__variable\" style=\"margin: 0px; padding: 0px; color: rgb(64, 64, 194);\">times</span>=<span class=\"php__string2\" style=\"margin: 0px; padding: 0px; color: fuchsia;\">&quot;1286861696&nbsp;&quot;</span>;&nbsp;<br style=\"margin: 0px; padding: 0px;\"/><span class=\"php__keyword\" style=\"margin: 0px; padding: 0px; color: navy; font-weight: bold;\">echo</span>&nbsp;tranTime(<span class=\"php__keyword\" style=\"margin: 0px; padding: 0px; color: navy; font-weight: bold;\">$</span><span class=\"php__variable\" style=\"margin: 0px; padding: 0px; color: rgb(64, 64, 194);\">times</span>);&nbsp;</code></pre><p><br/></p>', '1', '0', '1', '1', 'Win 8.1', '轮回', '124.152.7.106', '1451882738', '1492346014');

-- ----------------------------
-- Table structure for think_article_cate
-- ----------------------------
DROP TABLE IF EXISTS `think_article_cate`;
CREATE TABLE `think_article_cate` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '分类名称',
  `orderby` varchar(10) DEFAULT '100' COMMENT '排序',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_article_cate
-- ----------------------------
INSERT INTO `think_article_cate` VALUES ('1', '学习笔记', '1', '1477140627', '1480582693', '1');
INSERT INTO `think_article_cate` VALUES ('2', '生活随笔', '2', '1477140627', '1477140627', '1');
INSERT INTO `think_article_cate` VALUES ('3', '热点分享', '3', '1477140604', '1477140627', '1');
INSERT INTO `think_article_cate` VALUES ('4', '.NET', '4', '1477140627', '1477140627', '1');
INSERT INTO `think_article_cate` VALUES ('5', 'PHP', '5', '1477140627', '1477140627', '1');
INSERT INTO `think_article_cate` VALUES ('6', 'Java', '6', '1477140627', '1477140627', '1');

-- ----------------------------
-- Table structure for think_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_group`;
CREATE TABLE `think_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_auth_group
-- ----------------------------
INSERT INTO `think_auth_group` VALUES ('1', '超级管理员', '1', '', '1446535750', '1446535750');
INSERT INTO `think_auth_group` VALUES ('2', '内容管理员', '1', '1,2,9,10,11,12,3,30,31,32,33,34,4,35,36,37,38,39,5,6,7,8,27,28,29,13,14,22,24,25,40,41,42,43,26,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58', '1446535750', '1479305018');
INSERT INTO `think_auth_group` VALUES ('3', '系统维护员', '1', '1,2,9,10,11,12,3,30,31,32,33,34,4,35,36,37,38,39,5,6,7,8,27,28,29,13,14,22,24,25,40,41,42,43,26,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60', '1446535750', '1479879017');
INSERT INTO `think_auth_group` VALUES ('4', '系统测试员', '1', '1,2,9,10,11,12,3,30,31,32,33,34,4,35,36,37,38,39,61,62,5,6,7,8,27,28,29,13,14,22,24,25,40,41,42,43,26,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,67,68,69,70,71,72,73,74,80,75,76,77,78,79', '1446535750', '1487944011');

-- ----------------------------
-- Table structure for think_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_group_access`;
CREATE TABLE `think_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_auth_group_access
-- ----------------------------
INSERT INTO `think_auth_group_access` VALUES ('1', '1');
INSERT INTO `think_auth_group_access` VALUES ('9', '2');
INSERT INTO `think_auth_group_access` VALUES ('13', '4');

-- ----------------------------
-- Table structure for think_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_rule`;
CREATE TABLE `think_auth_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `css` varchar(20) NOT NULL COMMENT '样式',
  `condition` char(100) NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父栏目ID',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_auth_rule
-- ----------------------------
INSERT INTO `think_auth_rule` VALUES ('1', '#', '系统管理', '1', '1', 'fa fa-gear', '', '0', '1', '1446535750', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('2', 'admin/user/index', '用户管理', '1', '1', '', '', '1', '10', '1446535750', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('3', 'admin/role/index', '角色管理', '1', '1', '', '', '1', '20', '1446535750', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('4', 'admin/menu/index', '菜单管理', '1', '1', '', '', '1', '30', '1446535750', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('5', '#', '数据库管理', '1', '1', 'fa fa-database', '', '0', '2', '1446535750', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('6', 'admin/data/index', '数据库备份', '1', '1', '', '', '5', '50', '1446535750', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('7', 'admin/data/optimize', '优化表', '1', '1', '', '', '6', '50', '1477312169', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('8', 'admin/data/repair', '修复表', '1', '1', '', '', '6', '50', '1477312169', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('9', 'admin/user/useradd', '添加用户', '1', '1', '', '', '2', '50', '1477312169', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('10', 'admin/user/useredit', '编辑用户', '1', '1', '', '', '2', '50', '1477312169', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('11', 'admin/user/userdel', '删除用户', '1', '1', '', '', '2', '50', '1477312169', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('12', 'admin/user/user_state', '用户状态', '1', '1', '', '', '2', '50', '1477312169', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('13', '#', '日志管理', '1', '1', 'fa fa-tasks', '', '0', '6', '1477312169', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('14', 'admin/log/operate_log', '行为日志', '1', '1', '', '', '13', '50', '1477312169', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('22', 'admin/log/del_log', '删除日志', '1', '1', '', '', '14', '50', '1477312169', '1477316778');
INSERT INTO `think_auth_rule` VALUES ('24', '#', '文章管理', '1', '1', 'fa fa-paste', '', '0', '4', '1477312169', '1477312169');
INSERT INTO `think_auth_rule` VALUES ('25', 'admin/article/index_cate', '文章分类', '1', '1', '', '', '24', '10', '1477312260', '1477312260');
INSERT INTO `think_auth_rule` VALUES ('26', 'admin/article/index', '文章列表', '1', '1', '', '', '24', '20', '1477312333', '1477312333');
INSERT INTO `think_auth_rule` VALUES ('27', 'admin/data/import', '数据库还原', '1', '1', '', '', '5', '50', '1477639870', '1477639870');
INSERT INTO `think_auth_rule` VALUES ('28', 'admin/data/revert', '还原', '1', '1', '', '', '27', '50', '1477639972', '1477639972');
INSERT INTO `think_auth_rule` VALUES ('29', 'admin/data/del', '删除', '1', '1', '', '', '27', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('30', 'admin/role/roleAdd', '添加角色', '1', '1', '', '', '3', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('31', 'admin/role/roleEdit', '编辑角色', '1', '1', '', '', '3', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('32', 'admin/role/roleDel', '删除角色', '1', '1', '', '', '3', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('33', 'admin/role/role_state', '角色状态', '1', '1', '', '', '3', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('34', 'admin/role/giveAccess', '权限分配', '1', '1', '', '', '3', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('35', 'admin/menu/add_rule', '添加菜单', '1', '1', '', '', '4', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('36', 'admin/menu/edit_rule', '编辑菜单', '1', '1', '', '', '4', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('37', 'admin/menu/del_rule', '删除菜单', '1', '1', '', '', '4', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('38', 'admin/menu/rule_state', '菜单状态', '1', '1', '', '', '4', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('39', 'admin/menu/ruleorder', '菜单排序', '1', '1', '', '', '4', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('40', 'admin/article/add_cate', '添加分类', '1', '1', '', '', '25', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('41', 'admin/article/edit_cate', '编辑分类', '1', '1', '', '', '25', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('42', 'admin/article/del_cate', '删除分类', '1', '1', '', '', '25', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('43', 'admin/article/cate_state', '分类状态', '1', '1', '', '', '25', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('44', 'admin/article/add_article', '添加文章', '1', '1', '', '', '26', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('45', 'admin/article/edit_article', '编辑文章', '1', '1', '', '', '26', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('46', 'admin/article/del_article', '删除文章', '1', '1', '', '', '26', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('47', 'admin/article/article_state', '文章状态', '1', '1', '', '', '26', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('48', '#', '广告管理', '1', '1', 'fa fa-image', '', '0', '5', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('49', 'admin/ad/index_position', '广告位', '1', '1', '', '', '48', '10', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('50', 'admin/ad/add_position', '添加广告位', '1', '1', '', '', '49', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('51', 'admin/ad/edit_position', '编辑广告位', '1', '1', '', '', '49', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('52', 'admin/ad/del_position', '删除广告位', '1', '1', '', '', '49', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('53', 'admin/ad/position_state', '广告位状态', '1', '1', '', '', '49', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('54', 'admin/ad/index', '广告列表', '1', '1', '', '', '48', '20', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('55', 'admin/ad/add_ad', '添加广告', '1', '1', '', '', '54', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('56', 'admin/ad/edit_ad', '编辑广告', '1', '1', '', '', '54', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('57', 'admin/ad/del_ad', '删除广告', '1', '1', '', '', '54', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('58', 'admin/ad/ad_state', '广告状态', '1', '1', '', '', '54', '50', '1477640011', '1477640011');
INSERT INTO `think_auth_rule` VALUES ('61', 'admin/config/index', '配置管理', '1', '1', '', '', '1', '50', '1479908607', '1479908607');
INSERT INTO `think_auth_rule` VALUES ('62', 'admin/config/index', '配置列表', '1', '1', '', '', '61', '50', '1479908607', '1487943813');
INSERT INTO `think_auth_rule` VALUES ('63', 'admin/config/save', '保存配置', '1', '1', '', '', '61', '50', '1479908607', '1487943831');
INSERT INTO `think_auth_rule` VALUES ('70', '#', '会员管理', '1', '1', 'fa fa-users', '', '0', '3', '1484103066', '1484103066');
INSERT INTO `think_auth_rule` VALUES ('72', 'admin/member/add_group', '添加会员组', '1', '1', '', '', '71', '50', '1484103304', '1484103304');
INSERT INTO `think_auth_rule` VALUES ('71', 'admin/member/group', '会员组', '1', '1', '', '', '70', '10', '1484103304', '1484103304');
INSERT INTO `think_auth_rule` VALUES ('73', 'admin/member/edit_group', '编辑会员组', '1', '1', '', '', '71', '50', '1484103304', '1484103304');
INSERT INTO `think_auth_rule` VALUES ('74', 'admin/member/del_group', '删除会员组', '1', '1', '', '', '71', '50', '1484103304', '1484103304');
INSERT INTO `think_auth_rule` VALUES ('75', 'admin/member/index', '会员列表', '1', '1', '', '', '70', '20', '1484103304', '1484103304');
INSERT INTO `think_auth_rule` VALUES ('76', 'admin/member/add_member', '添加会员', '1', '1', '', '', '75', '50', '1484103304', '1484103304');
INSERT INTO `think_auth_rule` VALUES ('77', 'admin/member/edit_member', '编辑会员', '1', '1', '', '', '75', '50', '1484103304', '1484103304');
INSERT INTO `think_auth_rule` VALUES ('78', 'admin/member/del_member', '删除会员', '1', '1', '', '', '75', '50', '1484103304', '1484103304');
INSERT INTO `think_auth_rule` VALUES ('79', 'admin/member/member_status', '会员状态', '1', '1', '', '', '75', '50', '1484103304', '1487937671');
INSERT INTO `think_auth_rule` VALUES ('80', 'admin/member/group_status', '会员组状态', '1', '1', '', '', '71', '50', '1484103304', '1484103304');

-- ----------------------------
-- Table structure for think_config
-- ----------------------------
DROP TABLE IF EXISTS `think_config`;
CREATE TABLE `think_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `value` text COMMENT '配置值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_config
-- ----------------------------
INSERT INTO `think_config` VALUES ('1', 'web_site_title', '轮回后台管理系统');
INSERT INTO `think_config` VALUES ('2', 'web_site_description', '轮回后台管理系统');
INSERT INTO `think_config` VALUES ('3', 'web_site_keyword', '轮回后台管理系统');
INSERT INTO `think_config` VALUES ('4', 'web_site_icp', '陇ICP备15002349号-1');
INSERT INTO `think_config` VALUES ('5', 'web_site_cnzz', '');
INSERT INTO `think_config` VALUES ('6', 'web_site_copy', 'Copyright © 2017 轮回后台管理系统 All rights reserved.');
INSERT INTO `think_config` VALUES ('7', 'web_site_close', '1');
INSERT INTO `think_config` VALUES ('8', 'list_rows', '10');
INSERT INTO `think_config` VALUES ('9', 'admin_allow_ip', null);
INSERT INTO `think_config` VALUES ('10', 'alidayu_appkey', null);
INSERT INTO `think_config` VALUES ('11', 'alidayu_appSecret', null);

-- ----------------------------
-- Table structure for think_log
-- ----------------------------
DROP TABLE IF EXISTS `think_log`;
CREATE TABLE `think_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `admin_name` varchar(50) DEFAULT NULL COMMENT '用户姓名',
  `description` varchar(300) DEFAULT NULL COMMENT '描述',
  `ip` char(60) DEFAULT NULL COMMENT 'IP地址',
  `status` tinyint(1) DEFAULT NULL COMMENT '1 成功 2 失败',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3727 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_log
-- ----------------------------

INSERT INTO `think_log` VALUES ('3591', '13', 'test', '用户【test】登录失败：该账号被禁用', '203.198.38.88', '2', '1482137095');
INSERT INTO `think_log` VALUES ('3592', '1', 'admin', '用户【admin】登录失败：密码错误', '36.5.100.88', '2', '1482139531');
INSERT INTO `think_log` VALUES ('3593', '1', 'admin', '用户【admin】登录失败：密码错误', '36.5.100.88', '2', '1482139535');
INSERT INTO `think_log` VALUES ('3594', '1', 'admin', '用户【admin】登录失败：密码错误', '36.5.100.88', '2', '1482139536');
INSERT INTO `think_log` VALUES ('3595', '1', 'admin', '用户【admin】登录失败：密码错误', '36.5.100.88', '2', '1482139539');
INSERT INTO `think_log` VALUES ('3596', '1', 'admin', '用户【admin】登录失败：密码错误', '36.5.100.88', '2', '1482139541');
INSERT INTO `think_log` VALUES ('3597', '13', 'test', '用户【test】登录失败：该账号被禁用', '123.139.18.110', '2', '1482142027');
INSERT INTO `think_log` VALUES ('3598', '13', 'test', '用户【test】登录失败：该账号被禁用', '123.139.18.110', '2', '1482142040');
INSERT INTO `think_log` VALUES ('3599', '13', 'test', '用户【test】登录失败：该账号被禁用', '220.170.45.24', '2', '1482144407');
INSERT INTO `think_log` VALUES ('3600', '13', 'test', '用户【test】登录失败：该账号被禁用', '220.170.45.24', '2', '1482144431');
INSERT INTO `think_log` VALUES ('3601', '1', 'admin', '用户【admin】登录失败：密码错误', '220.170.45.24', '2', '1482144437');
INSERT INTO `think_log` VALUES ('3602', '1', 'admin', '用户【admin】登录失败：密码错误', '220.170.45.24', '2', '1482144441');
INSERT INTO `think_log` VALUES ('3603', '1', 'admin', '用户【admin】登录失败：密码错误', '220.170.45.24', '2', '1482144446');
INSERT INTO `think_log` VALUES ('3604', '13', 'test', '用户【test】登录失败：该账号被禁用', '120.82.79.142', '2', '1482147488');
INSERT INTO `think_log` VALUES ('3605', '13', 'test', '用户【test】登录失败：该账号被禁用', '120.82.79.142', '2', '1482147500');
INSERT INTO `think_log` VALUES ('3606', '13', 'test', '用户【test】登录失败：该账号被禁用', '120.82.79.142', '2', '1482147622');
INSERT INTO `think_log` VALUES ('3607', '13', 'test', '用户【test】登录失败：密码错误', '14.104.141.156', '2', '1482198106');
INSERT INTO `think_log` VALUES ('3608', '13', 'test', '用户【test】登录失败：密码错误', '14.104.141.156', '2', '1482198108');
INSERT INTO `think_log` VALUES ('3609', '13', 'test', '用户【test】登录失败：该账号被禁用', '14.104.141.156', '2', '1482198111');
INSERT INTO `think_log` VALUES ('3610', '13', 'test', '用户【test】登录失败：该账号被禁用', '14.104.141.156', '2', '1482198114');
INSERT INTO `think_log` VALUES ('3611', '13', 'test', '用户【test】登录失败：该账号被禁用', '14.104.141.156', '2', '1482198125');
INSERT INTO `think_log` VALUES ('3612', '13', 'test', '用户【test】登录失败：该账号被禁用', '113.116.77.169', '2', '1482198427');
INSERT INTO `think_log` VALUES ('3613', '1', 'admin', '用户【admin】登录失败：密码错误', '113.116.77.169', '2', '1482198445');
INSERT INTO `think_log` VALUES ('3614', '13', 'test', '用户【test】登录失败：该账号被禁用', '116.231.88.254', '2', '1482219010');
INSERT INTO `think_log` VALUES ('3615', '13', 'test', '用户【test】登录失败：该账号被禁用', '116.231.88.254', '2', '1482219014');
INSERT INTO `think_log` VALUES ('3616', '1', 'admin', '用户【admin】登录失败：密码错误', '116.231.88.254', '2', '1482219025');
INSERT INTO `think_log` VALUES ('3617', '13', 'test', '用户【test】登录失败：该账号被禁用', '60.171.83.90', '2', '1482220876');
INSERT INTO `think_log` VALUES ('3618', '13', 'test', '用户【test】登录失败：该账号被禁用', '180.173.174.92', '2', '1482222669');
INSERT INTO `think_log` VALUES ('3619', '13', 'test', '用户【test】登录失败：该账号被禁用', '180.173.174.92', '2', '1482222685');
INSERT INTO `think_log` VALUES ('3620', '13', 'test', '用户【test】登录失败：该账号被禁用', '203.198.38.88', '2', '1482222726');

-- ----------------------------
-- Table structure for think_member
-- ----------------------------
DROP TABLE IF EXISTS `think_member`;
CREATE TABLE `think_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(64) DEFAULT NULL COMMENT '邮件或者手机',
  `nickname` varchar(32) DEFAULT NULL COMMENT '昵称',
  `sex` int(10) DEFAULT NULL COMMENT '1男2女',
  `password` char(32) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `head_img` varchar(128) DEFAULT NULL COMMENT '头像',
  `integral` int(11) DEFAULT '0' COMMENT '积分',
  `money` int(11) DEFAULT '0' COMMENT '账户余额',
  `mobile` varchar(11) DEFAULT NULL COMMENT '认证的手机号码',
  `create_time` int(11) DEFAULT '0' COMMENT '注册时间',
  `update_time` int(11) DEFAULT NULL COMMENT '最后一次登录',
  `login_num` varchar(15) DEFAULT NULL COMMENT '登录次数',
  `status` tinyint(1) DEFAULT NULL COMMENT '1正常  0 禁用',
  `closed` tinyint(1) DEFAULT '0' COMMENT '0正常，1删除',
  `token` char(32) DEFAULT '0' COMMENT '令牌',
  `session_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=212065 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of think_member
-- ----------------------------
INSERT INTO `think_member` VALUES ('2', '1217037610', 'XiMi丶momo', '2', 'd41d8cd98f00b204e9800998ecf8427e', '1', '20161122\\ab9f9c492871857e1a6c5bc1c658ef7f.jpg', '300', '200', '18809321956', '1476779394', '1476779394', '0', '1', '1', '0', '');
INSERT INTO `think_member` VALUES ('1', '18809321929', '醉凡尘丶Wordly', '1', 'd41d8cd98f00b204e9800998ecf8427e', '1', '20161122\\admin.jpg', '92960', '73', '18809321929', '1476762875', '1476762875', '0', '1', '0', '0', '');
INSERT INTO `think_member` VALUES ('3', '1217037610', '紫陌轩尘', '1', 'd41d8cd98f00b204e9800998ecf8427e', '1', '20161122\\293c8cd05478b029a378ac4e5a880303.jpg', '400', '434', '49494', '1476676516', '1476676516', '0', '1', '1', '0', '');

-- ----------------------------
-- Table structure for think_member_group
-- ----------------------------
DROP TABLE IF EXISTS `think_member_group`;
CREATE TABLE `think_member_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '留言Id',
  `group_name` varchar(32) NOT NULL COMMENT '留言评论作者',
  `status` tinyint(1) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL COMMENT '留言回复时间',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COMMENT='文章评论表';

-- ----------------------------
-- Records of think_member_group
-- ----------------------------
INSERT INTO `think_member_group` VALUES ('1', '系统组', '1', '1441616559', null);
INSERT INTO `think_member_group` VALUES ('2', '游客组', '1', '1441617195', null);
INSERT INTO `think_member_group` VALUES ('3', 'VIP', '1', '1441769224', null);
INSERT INTO `think_member_group` VALUES ('4', 'VIPP', '1', '1443401058', null);
